public class Board {
	
	private die dice1;
	private die dice2;
	private boolean[] closedTiles;
	
	public Board() {
		this.dice1 = new die();
		this.dice2 = new die();
		this.closedTiles = new boolean[12];
	}
	
	public String toString() {
		String returnString = "";
		for (int i=0; i < this.closedTiles.length; i++) {
			returnString += this.closedTiles[i] ? i + " ": "X " ;
		}
		return returnString;
	}
	
	public boolean playATurn(){
		this.dice1.roll();
		this.dice2.roll();
		
		System.out.println(this.dice1);
		System.out.println(this.dice2);
		
		int totalPips = this.dice1.getPips() + this.dice2.getPips();
		
		if (!this.closedTiles[totalPips-1]) {
			this.closedTiles[totalPips-1] = true;
			System.out.println("Closing tile: " + (totalPips-1));
			return false;
		} else {
			System.out.println("Tile already shut");
			return true;
		}
	}
	
	
	
}