import java.util.Random;

public class die {
	
	private int pips;
	private Random rand;
	
	public die(){
		this.pips = 0;
		this.rand = new Random();
	}
	
	public int getPips() {
		return this.pips;
		
	}
	
	public void roll() {
		this.pips = this.rand.nextInt(1,6);
	}
	
	
	public String toString() {
		return Integer.toString(this.pips);
	}
	
}