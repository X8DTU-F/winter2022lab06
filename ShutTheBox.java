public class ShutTheBox {
	public static void main(String[] args) {
	System.out.println("Welcome to Shut the box, Enjoy!");
	
	boolean gameOver = false;
		
	Board boards = new Board();
	
	
	while (gameOver == false) {
		System.out.println("Player 1's turn!");
		System.out.println(boards);
		if (boards.playATurn()) {
			System.out.println("Player 2 wins!");
			gameOver = true;
		} else {
			System.out.println("Player 2's turn!");
			System.out.println(boards);	
			if (boards.playATurn()) {
				System.out.println("Player 1 wins!");
				gameOver = true;
			}
		}
	}
	}
		 
	
}